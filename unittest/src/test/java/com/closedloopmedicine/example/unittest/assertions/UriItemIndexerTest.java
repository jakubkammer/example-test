package com.closedloopmedicine.example.unittest.assertions;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.in;
import static org.assertj.core.api.Assumptions.assumeThat;

@RunWith(Theories.class)
public class UriItemIndexerTest {

    private final URI baseUri = URI.create("http://closedloopmedicine.com/example/warehouse");

    private UriItemIndexer indexer;

    @DataPoints
    public static ItemType[] ITEM_TYPES = ItemType.values();


    @Before
    public void setUp() throws Exception {
        indexer = new UriItemIndexer(baseUri);
    }

    @Theory
    public void shouldIncludeItemsStockIdInGeneratedIndexIdIfStockIdIsSetForItemType(ItemType itemType) {

        // given
        assumeThat(itemType.getStockId()).isNotEmpty();
        final String sku = "10009";

        // when
        String indexId = indexer.createId(sku, itemType);

        // then
        assertThat(indexId).contains("/warehouse/" + itemType.getStockId() + "/");
    }

    @Test
    public void shouldNotIncludeGeneralStockIdInGeneratedIndexIfItemTypeDoesNotHaveStockId() {

        // given
        final String sku = "10009";

        // when
        String indexId = indexer.createId(sku, ItemType.GENERAL);

        // then
        assertThat(indexId).endsWith("/warehouse/general/10009");
    }

    @Test
    public void shouldIncludeItemSkuInGeneratedIndexId() {

        // given
        final String sku = "665";

        // when
        String indexId = indexer.createId(sku, ItemType.ANCIENT_ARTIFACT);

        // then
        assertThat(indexId).endsWith("/665");
    }

    @Test
    public void shouldGenerateIndexIdsFromBaseWarehouseUri() {

        // given
        final String sku = "10009";

        // when
        String indexId = indexer.createId(sku, ItemType.ANCIENT_ARTIFACT);

        // then
        assertThat(indexId).isEqualTo("http://closedloopmedicine.com/example/warehouse/mystical/10009");
    }

}