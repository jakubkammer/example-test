package com.closedloopmedicine.example.unittest.theories;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assumptions.assumeThat;

@RunWith(Theories.class)
public class TadalafilDailySpecificationTest {

    private TadalafilDailySpecification tadalafilDailySpecification = TadalafilDailySpecification.SHARED_INSTANCE;

    @DataPoints
    public static Medicine[] MEDICINES = new Medicine[] {
            new Medicine(MedicineType.TADALAFIL, Dose.fromDescription("2.5mg")),
            new Medicine(MedicineType.TADALAFIL, Dose.fromDescription("5mg")),
            new Medicine(MedicineType.TADALAFIL, Dose.fromDescription("10mg")),
            new Medicine(MedicineType.TADALAFIL, Dose.fromDescription("20mg")),
            new Medicine(MedicineType.TADALAFIL, Dose.fromDescription("20mg")),
            new Medicine(MedicineType.AMLODIPINE, Dose.fromDescription("5mg")),
            new Medicine(MedicineType.AMLODIPINE, Dose.fromDescription("10mg"))
    };

    @Before
    public void setUp() {

    }

    @Theory
    public void shouldRejectHighDoseTadalafil(Medicine medicine) {
        // assuming
        assumeThat(medicine).extracting(Medicine::getType).isEqualTo(MedicineType.TADALAFIL);
        assumeThat(medicine.getDose().getMilligrams()).isGreaterThan(BigDecimal.valueOf(5));

        // when
        boolean accepted = tadalafilDailySpecification.test(medicine);

        // then
        assertThat(accepted).isFalse();
    }

    @Theory
    public void shouldAcceptLowDoseTadalafil(Medicine medicine) {
        // assuming
        assumeThat(medicine).extracting(Medicine::getType).isEqualTo(MedicineType.TADALAFIL);
        assumeThat(medicine.getDose().getMilligrams()).isLessThanOrEqualTo(BigDecimal.valueOf(5));

        // when
        boolean accepted = tadalafilDailySpecification.test(medicine);

        // then
        assertThat(accepted).isTrue();
    }

    @Theory
    public void shouldRejectMedicineWhichIsNotTadalafil(Medicine medicine) {
        // assuming
        assumeThat(medicine).extracting(Medicine::getType).isNotEqualTo(MedicineType.TADALAFIL);

        // when
        boolean accepted = tadalafilDailySpecification.test(medicine);

        // then
        assertThat(accepted).isFalse();
    }

    @Test
    public void shouldThrowExceptionWhenMedicineIsNotDefined() {
        // when
        Throwable thrown = catchThrowable(() -> tadalafilDailySpecification.test(null));

        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
    }
}