package com.closedloopmedicine.example.unittest.assertions;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class OfficeSupplierTest {

    @Captor
    private ArgumentCaptor<List<PurchaseOrder>> purchaseOrdersArg;

    @Mock
    private Procurement procurement;

    @InjectMocks
    private OfficeSupplier officeSupplier;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldSubmitPurchaseOrdersToAllRegisteredOffices() {

        // given
        final List<String> items = Collections.singletonList("Coffee");

        officeSupplier.registerOffice("Cleveland downtown");
        officeSupplier.registerOffice("London Kensington");

        // when
        officeSupplier.sendSupplies(items);

        // then
        assertThat(submittedOrders())
                .extracting(PurchaseOrder::getDeliveryLocation)
                .containsExactly("Cleveland downtown", "London Kensington");
    }

    @Test
    public void shouldOrderTheSameItemsToEachRegisteredOffice() {

        // given
        final List<String> items = Arrays.asList("Coffee", "Fresh fruit");

        officeSupplier.registerOffice("Cleveland downtown");
        officeSupplier.registerOffice("London Kensington");

        // when
        officeSupplier.sendSupplies(items);

        // then
        assertItemOrdersSubmittedTo(items, "Cleveland downtown", "London Kensington");
    }

    private List<PurchaseOrder> submittedOrders() {
        verify(procurement).submitOrders(purchaseOrdersArg.capture());
        return purchaseOrdersArg.getValue();
    }

    private void assertItemOrdersSubmittedTo(List<String> items, String... officeLocations) {

        List<PurchaseOrder> orders = submittedOrders();
        Map<String, PurchaseOrder> ordersForLocations = orders.stream()
                .collect(Collectors.toMap(PurchaseOrder::getDeliveryLocation, Function.identity()));

        SoftAssertions softly = new SoftAssertions();
        Stream.of(officeLocations).map(ordersForLocations::get)
                .forEach(purchaseOrder -> {
                    softly.assertThat(purchaseOrder.getItems())
                            .describedAs("Items of purchase order \"" + purchaseOrder.getDeliveryLocation() + "\"")
                            .containsExactlyInAnyOrderElementsOf(items);
                });

        softly.assertAll();
    }
}