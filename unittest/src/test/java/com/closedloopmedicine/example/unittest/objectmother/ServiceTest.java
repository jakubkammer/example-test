package com.closedloopmedicine.example.unittest.objectmother;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class ServiceTest {

    private Service service;

    private MSISDN msisdn;
    private Account owner;

    @Before
    public void setUp() {
        msisdn = new MSISDN("079222999111");
        owner = mock(Account.class);
    }

    @Test
    public void shouldInitialiseInSuspendedState() {

        // when
        service = new Service(msisdn, owner);

        // then
        assertThat(service.getStatus()).isEqualTo(ServiceStatus.SUSPENDED);
    }

    @Test
    public void shouldAllowActivatingService() {

        // given
        service = new Service(msisdn, owner);

        // when
        service.activate();

        // then
        assertThat(service.getStatus()).isEqualTo(ServiceStatus.ACTIVE);
    }

    @Test
    public void shouldAllowSuspendingService() {

        // given
        service = new Service(msisdn, owner);
        service.activate();

        // when
        service.suspend();

        // then
        assertThat(service.getStatus()).isEqualTo(ServiceStatus.SUSPENDED);
    }
}