package com.closedloopmedicine.example.unittest.assertions;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderDispatcherTest {

    @Mock
    private Warehouse warehouse;

    @Mock
    private ItemIndexer indexer;

    @Captor
    private ArgumentCaptor<Item> itemArg;

    @InjectMocks
    private OrderDispatcher orderDispatcher;

    @Before
    public void setUp() throws Exception {

        when(warehouse.hasCapacity(anyInt())).thenReturn(true);
        when(indexer.nextItemId()).thenReturn("-1");
    }

    @Test
    public void shouldAddItemsToTheWarehouse() {
        // given
        List<String> itemNames = Arrays.asList("Old books", "Bicycle", "The Ark of the Covenant");

        // when
        orderDispatcher.acceptItems(itemNames);

        // then
        verify(warehouse, times(3)).addItem(itemArg.capture());
        assertThat(itemArg.getAllValues())
                .extracting(Item::getName)
                .containsExactlyElementsOf(itemNames);
    }

    @Test
    public void shouldAssignIndexValuesToItemsAddedToWarehouse() {
        // given
        List<String> itemNames = Arrays.asList("Old books", "Bicycle", "The Ark of the Covenant");
        when(indexer.nextItemId()).thenReturn("AA-01", "AA-02", "AA-03");

        // when
        orderDispatcher.acceptItems(itemNames);

        // then
        verify(warehouse, times(3)).addItem(itemArg.capture());
        assertThat(itemArg.getAllValues())
                .extracting(Item::getIndex)
                .containsExactly("AA-01", "AA-02", "AA-03");
    }

    @Test
    public void shouldThrowExceptionIfThereIsNoRoomInWarehouseToStoreAllTheItems() {
        // given
        List<String> itemNames = Arrays.asList("Old books", "Bicycle", "The Ark of the Covenant");
        when(warehouse.hasCapacity(3)).thenReturn(false);

        // when
        Throwable thrown = catchThrowable(() -> orderDispatcher.acceptItems(itemNames));

        // then
        assertThat(thrown).isInstanceOf(WarehouseOutOfRoomException.class);
    }

    @Test
    public void shouldCheckWarehouseCapacityForAllNewItems() {
        // given
        List<String> itemNames = Arrays.asList("Old books");

        // when
        orderDispatcher.acceptItems(itemNames);

        // then
        verify(warehouse).hasCapacity(1);
    }

    @Test
    public void shouldNotCheckWarehouseCapacityIfItemListIsEmpty() {
        // given
        List<String> itemNames = Collections.emptyList();

        // when
        orderDispatcher.acceptItems(itemNames);

        // then
        verifyZeroInteractions(warehouse);
    }

    @Test
    public void shouldRemoveAllItemsWithTheGivenName() {
        // given
        final String itemName = "Candlestick";

        // when
        orderDispatcher.removeItems(itemName);

        // then
        verify(warehouse).removeAllItemsByName(itemName);
        verifyNoMoreInteractions(warehouse);  // Unnecessary! No-interactions verification is not the focus
                                              // of this test
    }
}