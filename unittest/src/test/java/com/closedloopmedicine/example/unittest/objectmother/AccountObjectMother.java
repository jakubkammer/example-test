package com.closedloopmedicine.example.unittest.objectmother;

public class AccountObjectMother {

    private static final boolean ACTIVE = true;
    private static final boolean SUSPENDED = false;

    public static final MSISDN JOHN_SMITH_MSISDN_1 = new MSISDN("07919656847");
    public static final MSISDN JOHN_SMITH_MSISDN_2 = new MSISDN("07917994341");
    public static final MSISDN JOHN_SMITH_MSISDN_3 = new MSISDN("07917785432");

    /**
     * Existing customer with multiple services.
     */
    public Account johnSmith() {
        Person johnSmith = new Person("John", "Smith");
        Account johnSmithAccount = new Account(johnSmith);

        Service service1 = serviceInState(JOHN_SMITH_MSISDN_1, johnSmithAccount, ACTIVE);
        Service service2 = serviceInState(JOHN_SMITH_MSISDN_2, johnSmithAccount, SUSPENDED);
        Service service3 = serviceInState(JOHN_SMITH_MSISDN_3, johnSmithAccount, ACTIVE);

        johnSmithAccount.addService(service1);
        johnSmithAccount.addService(service2);
        johnSmithAccount.addService(service3);

        return johnSmithAccount;
    }

    /**
     * New customer with no services
     */
    public Account toddAngel() {
        Person toddAngel = new Person("Todd", "Angel");
        Account toddAngelAccount = new Account(toddAngel);

        return toddAngelAccount;
    }

    private Service serviceInState(MSISDN msisdn, Account owner, boolean state) {
        Service service = new Service(msisdn, owner);
        if (state) {
            service.activate();
        } else {
            service.suspend();
        }
        return service;
    }
}
