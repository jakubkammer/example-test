package com.closedloopmedicine.example.unittest.assertions;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BinaryCounterTest {

    private BinaryCounter binaryCounter;

    @Before
    public void setUp() throws Exception {
        binaryCounter = BinaryCounter.SHARED_INSTANCE;
    }

    @Test
    public void testCountsNumberOfSetBitsInValue() {

        assertThat(binaryCounter.countBits(1)).isEqualTo(1);
        assertThat(binaryCounter.countBits(2)).isEqualTo(1);
        assertThat(binaryCounter.countBits(0b1101)).isEqualTo(3);
        assertThat(binaryCounter.countBits(0b10000000000000000000000000000001)).isEqualTo(2);
    }
}