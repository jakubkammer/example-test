package com.closedloopmedicine.example.unittest.assertions;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CoffeeRepositoryTest {

    private static final AtomicInteger SKU_ID_SEQ = new AtomicInteger(1);

    @Mock
    private BeverageRepository beverageRepository;

    @Captor
    private ArgumentCaptor<Beverage> beverageArg;

    @InjectMocks
    private CoffeeRepository coffeeRepository = new CoffeeRepository(SKU_ID_SEQ);

    @Before
    public void setUp() {
        when(beverageRepository.save(beverageArg.capture())).thenReturn(1);
    }

    @Test
    public void shouldAddBeverageWithCoffeeNameToBeverageRepository() {

        // given
        final String coffeeName = "Java";

        // when
        coffeeRepository.addCoffee(coffeeName);

        // then
        assertThat(beverageArg.getValue().getName()).isEqualTo("Coffee (Java)");
    }

    @Test
    public void shouldAssignSkuIdsToCoffeeBeverages() {

        // given
        final String coffeeName = "Java";

        // when
        coffeeRepository.addCoffee(coffeeName);

        // then
        assertThat(beverageArg.getValue().getSku()).isEqualTo("COFF02");
    }

    @Test
    public void shouldNotAddBeverageIfCoffeeNameIsEmpty() {

        // given
        final String coffeeName = "";

        // when
        coffeeRepository.addCoffee(coffeeName);

        // then
        verify(beverageRepository, never()).save(any(Beverage.class));
    }
}