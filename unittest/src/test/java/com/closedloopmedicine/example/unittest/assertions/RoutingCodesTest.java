package com.closedloopmedicine.example.unittest.assertions;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.catchThrowable;

public class RoutingCodesTest {

    private RoutingCodes routingCodes;

    @Before
    public void setUp() {
        routingCodes = new RoutingCodes();
    }

    @Test
    public void shouldAllocateDifferentRoutingCodesForDifferentCountries() throws Exception {

        // given
        routingCodes.addCountry("Vatican");
        routingCodes.addCountry("China");
        routingCodes.addCountry("Brazil");

        // when
        String vaticanCode = routingCodes.lookup("Vatican");
        String chinaCode = routingCodes.lookup("China");
        String brazilCode = routingCodes.lookup("Brazil");

        // then
        assertThat(vaticanCode).isNotEqualTo(chinaCode).isNotEqualTo(brazilCode);
        assertThat(chinaCode).isNotEqualTo(brazilCode);
    }

    @Test
    public void shouldThrowUnknownCountryExceptionWhenLookingUpCodeIfCountryHasNotBeenAdded() {

        // given
        routingCodes.addCountry("Vatican");

        // when
        Throwable thrown = catchThrowable(() -> routingCodes.lookup("Sweden"));

        // then
        assertThat(thrown)
                .isInstanceOf(UnknownCountryException.class)
                .extracting("countryName")
                .isEqualTo("Sweden");
    }

    @Test
    public void shouldThrowExceptionWhenLookingUpCodeIfCountryNameIsNull() {

        assertThatThrownBy(() -> routingCodes.lookup(null))
                .isInstanceOf(NullPointerException.class);
    }
}