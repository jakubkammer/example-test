package com.closedloopmedicine.example.unittest.assertions;

import org.junit.Before;
import org.junit.Test;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

public class WarehouseUriBuilderTest {

    private final static URI EXAMPLE_TEST_URI = URI.create("http://example.com/test");

    private WarehouseUriBuilder builder;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldAllowAppendingTheGivenPathToTheCurrentUri() {

        // given
        builder = new WarehouseUriBuilder(EXAMPLE_TEST_URI);

        // when
        builder.addPath("foo");

        // then
        assertThat(builder.toString()).isEqualTo("http://example.com/test/foo");
    }

    @Test
    public void shouldAllowAppendingMultiplePathsToTheCurrentUri() {

        // given
        builder = new WarehouseUriBuilder(EXAMPLE_TEST_URI);

        // when
        builder.addPath("foo");
        builder.addPath("bar");

        // then
        assertThat(builder.toString()).isEqualTo("http://example.com/test/foo/bar");
    }

    @Test
    public void shouldIgnoreAddedEmptyPaths() {

        // given
        builder = new WarehouseUriBuilder(EXAMPLE_TEST_URI);

        // when
        builder.addPath("");

        // then
        assertThat(builder.toString()).isEqualTo("http://example.com/test");
    }

    @Test
    public void shouldIgnoreAddedNullPaths() {

        // given
        builder = new WarehouseUriBuilder(EXAMPLE_TEST_URI);

        // when
        builder.addPath(null);

        // then
        assertThat(builder.toString()).isEqualTo("http://example.com/test");
    }
}