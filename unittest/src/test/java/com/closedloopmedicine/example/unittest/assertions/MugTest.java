package com.closedloopmedicine.example.unittest.assertions;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MugTest {

    private class TestMug extends Mug {

        public TestMug(int capacityMl) {
            super(capacityMl);
        }

        @Override
        protected void verifyCompatibleLiquid(Liquid liquid) {
            delegateMug.verifyCompatibleLiquid(liquid);
        }
    }

    private Mug delegateMug;
    private Liquid liquid;

    private TestMug testMug;

    @Before
    public void setUp() {
        delegateMug = mock(Mug.class);

        liquid = mock(Liquid.class);
    }

    @Test
    public void shouldAcceptVolumeOfLiquidSmallerThanMugCapacity() {

        // given
        testMug = new TestMug(100);

        // when
        testMug.pourIn(liquid, 99);

        // then
    }

    @Test
    public void shouldAcceptVolumeOfLiquidEqualToMugCapacity() {

        // given
        testMug = new TestMug(100);

        // when
        testMug.pourIn(liquid, 100);

        // then
    }

    @Test
    public void shouldAllowPouringInLiquidAsLongAsTotalVolumeIsLessOrEqualToMugCapacity() {

        // given
        testMug = new TestMug(100);

        // when
        testMug.pourIn(liquid, 10);
        testMug.pourIn(liquid, 20);
        testMug.pourIn(liquid, 30);
        testMug.pourIn(liquid, 40);

        // then
    }

    @Test
    public void shouldThrowOverflowExceptionWhenPouringInVolumeOfLiquidGreaterThanMugCapacity() {

        // given
        testMug = new TestMug(100);

        // when
        Throwable thrown = catchThrowable(() -> testMug.pourIn(liquid, 101));

        // then
        assertThat(thrown).isInstanceOf(OverflowException.class);
    }

    @Test
    public void shouldThrowOverflowExceptionWhenSubsequentPouringOfLiquidIExceedsMugCapacity() {

        // given
        testMug = new TestMug(100);

        // when
        testMug.pourIn(liquid, 10);
        testMug.pourIn(liquid, 20);
        testMug.pourIn(liquid, 30);
        testMug.pourIn(liquid, 40);
        Throwable thrown = catchThrowable(() -> testMug.pourIn(liquid, 1));

        // then
        assertThat(thrown).isInstanceOf(OverflowException.class);
    }

    @Test
    public void shouldVerifyCompatibilityOfLiquidAndMug() {

        // given
        testMug = new TestMug(100);

        // when
        testMug.pourIn(liquid, 100);

        // then
        verify(delegateMug).verifyCompatibleLiquid(liquid);
    }

    @Test
    public void shouldVerifyLiquidCompatibilityBeforeCheckingOverflow() {

        // given
        RuntimeException coffeeException = new RuntimeException("can't pour coffee in") {};
        doThrow(coffeeException).when(delegateMug).verifyCompatibleLiquid(liquid);
        testMug = new TestMug(100);

        // when
        Throwable thrown = catchThrowable(() -> testMug.pourIn(liquid, 101));

        // then
        assertThat(thrown).isSameAs(coffeeException);
    }
}