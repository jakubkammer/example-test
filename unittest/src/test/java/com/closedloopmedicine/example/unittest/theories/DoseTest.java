package com.closedloopmedicine.example.unittest.theories;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class DoseTest {

    private Dose dose;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldConvertDoseToMicrograms() {

        // given
        dose = new Dose(BigDecimal.valueOf(2.5));

        // when
        BigDecimal micrograms = dose.inMicrograms();

        // then
        assertThat(micrograms).isEqualByComparingTo("2500");
    }

    @Test
    public void shouldCreateDoseFromMilligramDescription() {

        // when
        dose = Dose.fromDescription("120mg");

        // then
        assertThat(dose.getMilligrams()).isEqualByComparingTo("120");
    }

    @Test
    public void shouldThrowExceptionWhenCreatingDoseFromDescriptionIfDescriptionFormatIsNotRecognised() {

        assertThatThrownBy(() -> Dose.fromDescription("0.003kg"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("0.003kg");
    }
}