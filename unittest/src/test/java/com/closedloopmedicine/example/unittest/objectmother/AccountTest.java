package com.closedloopmedicine.example.unittest.objectmother;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountTest {

    private AccountObjectMother accountMother = new AccountObjectMother();

    private Account account;

    @Test
    public void shouldSuspendAllServicesWhenAccountIsSuspended() {

        // given
        account = accountMother.johnSmith();

        // when
        account.suspend();

        // then
        assertThat(account.getServices())
                .extracting(Service::getStatus)
                .containsOnly(ServiceStatus.SUSPENDED);
    }

    @Test
    public void shouldNotModifyAccountStateIfAccountWithoutServicesIsSuspended() {

        // given
        account = accountMother.toddAngel();

        // when
        account.suspend();

        // then
        assertThat(account.getServices()).isEmpty();
    }

    @Test
    public void shouldAddServicesToNewAccounts() {

        // given
        account = accountMother.toddAngel();
        Service newService = new Service(new MSISDN("098888111222"), account);

        // when
        account.addService(newService);

        // then
        assertThat(account.getServices())
                .containsExactly(newService);
    }

    @Test
    public void shouldAddServicesToAccountsWithExistingServices() {

        // given
        account = accountMother.johnSmith();
        MSISDN newServiceMsisdn = new MSISDN("098888111222");
        Service newService = new Service(newServiceMsisdn, account);

        // when
        account.addService(newService);

        // then
        assertThat(account.getServices())
                .hasSize(4)
                .extracting(Service::getMsisdn)
                .containsExactly(AccountObjectMother.JOHN_SMITH_MSISDN_1,
                        AccountObjectMother.JOHN_SMITH_MSISDN_2,
                        AccountObjectMother.JOHN_SMITH_MSISDN_3,
                        newServiceMsisdn);
    }

    @Test
    public void shouldReturnEmptyServiceListForNewAccounts() {

        // given
        account = accountMother.toddAngel();

        // then
        assertThat(account.getServices()).isEmpty();
    }
}