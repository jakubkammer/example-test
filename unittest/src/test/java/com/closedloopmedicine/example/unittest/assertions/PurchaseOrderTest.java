package com.closedloopmedicine.example.unittest.assertions;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PurchaseOrderTest {

    private PurchaseOrder order;

    @Test
    public void shouldCreateInitiallyEmptyOrder() {

        // when
        order = new PurchaseOrder("Cleveland");

        // then
        assertThat(order.getItems()).isEmpty();
    }

    @Test
    public void shouldAllowAddingItems() {

        // given
        order = new PurchaseOrder("Cleveland");

        // when
        order.addItem("Coffee");
        order.addItem("Chalk");

        // then
        assertThat(order.getItems()).containsExactlyInAnyOrder("Coffee", "Chalk");
    }
}