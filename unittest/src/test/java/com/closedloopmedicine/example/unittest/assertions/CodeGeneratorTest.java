package com.closedloopmedicine.example.unittest.assertions;

import org.assertj.core.data.Offset;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CodeGeneratorTest {

    private CodeGenerator generator;

    @Before
    public void setUp() throws Exception {
        generator = new CodeGenerator();
    }

    @Test
    public void shouldGenerateUniqueCodes() {

        // given
        final int iterations = 10000;
        final List<String> codes = new ArrayList<>(iterations);

        // when
        for (int i = 0; i < iterations; i++) {
            codes.add(generator.next());
        }

        // then
        assertThat(new HashSet<>(codes)).hasSameSizeAs(codes);
    }

    @Test
    public void shouldGenerateNonSequentialCodes() {

        // given
        final int iterations = 10000;
        final List<Integer> codesAsInteger = new ArrayList<>(iterations);

        // when
        for (int i = 0; i < iterations; i++) {
            codesAsInteger.add(Integer.valueOf(generator.next()));
        }

        // then
        for (int i = 0; i < iterations - 1; i++) {
            int code = codesAsInteger.get(i);
            int nextCode = codesAsInteger.get(i + 1);

            assertThat(code).isNotCloseTo(nextCode, Offset.offset(3));
        }
    }
}