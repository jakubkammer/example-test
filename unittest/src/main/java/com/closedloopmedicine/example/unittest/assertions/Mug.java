package com.closedloopmedicine.example.unittest.assertions;

public abstract class Mug {

    private int capacityMl;
    private int liquidVolumeMl;

    public Mug(int capacityMl) {
        this.capacityMl = capacityMl;
    }

    public void pourIn(Liquid liquid, int volumeMl) {

        verifyCompatibleLiquid(liquid);

        if (volumeMl > (capacityMl - liquidVolumeMl)) {
            throw new OverflowException();
        }
        liquidVolumeMl += volumeMl;
    }

    protected abstract void verifyCompatibleLiquid(Liquid liquid);
}
