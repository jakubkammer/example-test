package com.closedloopmedicine.example.unittest.objectmother;

public class Service {

    private MSISDN msisdn;
    private Account owner;
    private ServiceStatus status = ServiceStatus.SUSPENDED;

    public Service(MSISDN msisdn, Account owner) {
        this.msisdn = msisdn;
        this.owner = owner;
    }

    public void activate() {
        status = ServiceStatus.ACTIVE;
    }

    public void suspend() {
        status = ServiceStatus.SUSPENDED;
    }

    public ServiceStatus getStatus() {
        return status;
    }

    public MSISDN getMsisdn() {
        return msisdn;
    }
}
