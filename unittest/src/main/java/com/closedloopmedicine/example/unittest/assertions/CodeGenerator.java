package com.closedloopmedicine.example.unittest.assertions;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CodeGenerator {

    private final int INPUT_VALUE_HASH = 0xFFF000;
    private final int OUTPUT_VALUE_HASH = 0b101101101010110101001101;
    private final int MASK = 0xFFFFFF;
    private final int BIT_COUNT = 24;

    private AtomicInteger counter = new AtomicInteger(0);

    private int[] bitValues = new int[BIT_COUNT];

    public CodeGenerator() {
        List<Integer> bitOrderList = IntStream.range(0, BIT_COUNT).boxed().collect(Collectors.toList());
        Collections.shuffle(bitOrderList);

        for (int i = 0; i < BIT_COUNT; i++) {
            bitValues[i] = 1 << bitOrderList.get(i);
        }
    }

    public String next() {

        int inputValue = counter.getAndIncrement() ^ INPUT_VALUE_HASH;
        int withRandomizedBits = 0;
        for (int bit = 0; bit < BIT_COUNT; bit++) {
            boolean isSet = (inputValue & 1) == 1;
            inputValue >>= 1;
            if (isSet) {
                withRandomizedBits |= bitValues[bit];
            }
        }
        return String.valueOf(withRandomizedBits ^ OUTPUT_VALUE_HASH);
    }
}
