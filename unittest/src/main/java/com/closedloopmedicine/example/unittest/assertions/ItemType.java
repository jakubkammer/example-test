package com.closedloopmedicine.example.unittest.assertions;

public enum ItemType {

    HOUSEWARE("houseware"),
    FOODSTUFF("food"),
    ANCIENT_ARTIFACT("mystical"),
    GENERAL("");

    ItemType(String stockId) {
        this.stockId = stockId;
    }

    private String stockId;

    public String getStockId() {
        return stockId;
    }
}
