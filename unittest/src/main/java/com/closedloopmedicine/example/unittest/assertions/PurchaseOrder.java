package com.closedloopmedicine.example.unittest.assertions;

import java.util.HashSet;
import java.util.Set;

public class PurchaseOrder {
    private String deliveryLocation;

    private Set<String> items;

    public PurchaseOrder(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
        this.items = new HashSet<>();
    }

    public String getDeliveryLocation() {
        return deliveryLocation;
    }

    public Set<String> getItems() {
        return items;
    }

    public void addItem(String item) {
        items.add(item);
    }
}
