package com.closedloopmedicine.example.unittest.assertions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OfficeSupplier {

    private Procurement procurement;

    private List<String> registeredOffices = new ArrayList<>();

    public void registerOffice(String officeLocation) {
        registeredOffices.add(officeLocation);
    }

    public void sendSupplies(List<String> items) {

        List<PurchaseOrder> purchaseOrders = registeredOffices.stream()
                .map(officeLocation -> createForLocationWithItems(officeLocation, items))
                .collect(Collectors.toList());
        procurement.submitOrders(purchaseOrders);
    }

    private PurchaseOrder createForLocationWithItems(String officeLocation, List<String> items) {
        PurchaseOrder order = new PurchaseOrder(officeLocation);
        items.forEach(order::addItem);
        return order;
    }
}
