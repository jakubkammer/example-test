package com.closedloopmedicine.example.unittest.assertions;

import java.util.concurrent.atomic.AtomicInteger;

public class CoffeeRepository {

    private BeverageRepository beverageRepository;

    private AtomicInteger skuIdSeq;

    public CoffeeRepository(AtomicInteger skuIdSeq) {
        this.skuIdSeq = skuIdSeq;
    }

    public void addCoffee(String name) {

        if (name.length() == 0) {
            return;
        }

        Beverage beverage = new Beverage();
        beverage.setSku(generateCoffeeSkuId());
        beverage.setName("Coffee (" + name + ")");

        beverageRepository.save(beverage);
    }

    private String generateCoffeeSkuId() {
        return String.format("COFF%02d", skuIdSeq.getAndIncrement());
    }
}
