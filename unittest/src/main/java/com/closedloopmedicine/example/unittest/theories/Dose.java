package com.closedloopmedicine.example.unittest.theories;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dose {

    private static final Pattern MILLIGRAM_DOSE_DESCRIPTION = Pattern.compile("(\\d+)mg");

    private BigDecimal milligrams;

    public Dose(BigDecimal milligrams) {
        this.milligrams = milligrams;
    }

    public static Dose fromDescription(String doseDescription) {
        Matcher milligramDoseMatcher = MILLIGRAM_DOSE_DESCRIPTION.matcher(doseDescription);
        if (milligramDoseMatcher.find()) {
            return new Dose(new BigDecimal(milligramDoseMatcher.group(1)));
        }

        throw new IllegalArgumentException(doseDescription);
    }

    public BigDecimal getMilligrams() {
        return milligrams;
    }

    public BigDecimal inMicrograms() {
        return milligrams.movePointRight(3);
    }
}
