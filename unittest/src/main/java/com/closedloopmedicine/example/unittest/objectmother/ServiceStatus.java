package com.closedloopmedicine.example.unittest.objectmother;

public enum ServiceStatus {
    ACTIVE,
    SUSPENDED
}
