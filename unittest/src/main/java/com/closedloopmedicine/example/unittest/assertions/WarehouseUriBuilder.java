package com.closedloopmedicine.example.unittest.assertions;

import org.apache.http.client.utils.URIBuilder;

import java.net.URI;

public class WarehouseUriBuilder extends URIBuilder {
    public WarehouseUriBuilder(URI uri) {
        super(uri);
    }

    public WarehouseUriBuilder addPath(String pathElement) {
        if (pathElement == null || pathElement.length() == 0) {
            return this;
        }

        setPath(getPath() + "/" + pathElement);
        return this;
    }
}
