package com.closedloopmedicine.example.unittest.theories;

import java.math.BigDecimal;
import java.util.function.Predicate;

public class TadalafilDailySpecification implements Predicate<Medicine> {

    public static final TadalafilDailySpecification SHARED_INSTANCE = new TadalafilDailySpecification();

    private static final BigDecimal MAX_DAILY_DOSE_IN_MCG = new BigDecimal(5000);

    @Override
    public boolean test(Medicine medicine) {
        if (medicine == null) {
            throw new IllegalArgumentException("medicine is not defined");
        }

        if (medicine.getDose().inMicrograms().compareTo(MAX_DAILY_DOSE_IN_MCG) > 0) {
            return false;
        }
        if (medicine.getType() != MedicineType.TADALAFIL) {
            return false;
        }
        return true;
    }
}
