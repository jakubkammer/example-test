package com.closedloopmedicine.example.unittest.assertions;

import java.net.URI;

public class UriItemIndexer {

    private static final String GENERAL_ITEM_STOCK_ID = "general";

    private URI baseUri;

    public UriItemIndexer(URI warehouseUri) {
        this.baseUri = warehouseUri;
    }

    public String createId(String sku, ItemType itemType) {

        return new WarehouseUriBuilder(baseUri)
                .addPath(getWarehouseStockId(itemType.getStockId()))
                .addPath(sku)
                .toString();
    }

    private String getWarehouseStockId(String itemStockId) {
        return itemStockId.isEmpty() ? GENERAL_ITEM_STOCK_ID : itemStockId;
    }
}
