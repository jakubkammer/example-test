package com.closedloopmedicine.example.unittest.assertions;

import java.util.Collection;

public class OrderDispatcher {

    private Warehouse warehouse;

    private ItemIndexer itemIndexer;

    public void acceptItems(Collection<String> names) {

        if (names.isEmpty()) {
            return;
        }

        if (!warehouse.hasCapacity(names.size())) {
            throw new WarehouseOutOfRoomException("No room left in the warehouse");
        }

        names.stream()
                .map(name -> new Item(name, itemIndexer.nextItemId()))
                .forEach(warehouse::addItem);
    }

    public void removeItems(String itemName) {

        warehouse.removeAllItemsByName(itemName);
    }
}
