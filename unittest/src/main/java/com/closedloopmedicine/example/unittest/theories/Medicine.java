package com.closedloopmedicine.example.unittest.theories;

public class Medicine {

    private final MedicineType type;
    private final Dose dose;

    public Medicine(MedicineType type, Dose dose) {
        this.type = type;
        this.dose = dose;
    }

    public MedicineType getType() {
        return type;
    }

    public Dose getDose() {
        return dose;
    }
}
