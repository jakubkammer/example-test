package com.closedloopmedicine.example.unittest.assertions;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RoutingCodes {

    private Map<String, String> countryNameToRoutingCode = new HashMap<>();

    public void addCountry(String countryName) {
        countryNameToRoutingCode.put(countryName, generateCodeFor(countryName));
    }

    private String generateCodeFor(String countryName) {
        return String.format("%s%03d", countryName.substring(2), countryNameToRoutingCode.size());
    }

    public String lookup(String countryName) throws UnknownCountryException {

        Objects.requireNonNull(countryName, "country name is missing");

        String code = countryNameToRoutingCode.get(countryName);

        if (code == null) {
            throw new UnknownCountryException(countryName);
        }
        return code;
    }
}
