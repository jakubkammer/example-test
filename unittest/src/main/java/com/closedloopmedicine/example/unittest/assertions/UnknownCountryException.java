package com.closedloopmedicine.example.unittest.assertions;

public class UnknownCountryException extends Exception {

    private String countryName;

    public UnknownCountryException(String countryName) {
        super("Unknown country: \"" + countryName + "\"");
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }
}
