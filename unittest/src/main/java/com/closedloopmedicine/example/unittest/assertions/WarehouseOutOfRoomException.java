package com.closedloopmedicine.example.unittest.assertions;

public class WarehouseOutOfRoomException extends RuntimeException {

    public WarehouseOutOfRoomException(String message) {
        super(message);
    }
}
