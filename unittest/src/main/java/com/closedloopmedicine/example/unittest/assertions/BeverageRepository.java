package com.closedloopmedicine.example.unittest.assertions;

public interface BeverageRepository {

    Integer save(Beverage beverage);
}
