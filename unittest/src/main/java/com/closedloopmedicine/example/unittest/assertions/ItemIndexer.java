package com.closedloopmedicine.example.unittest.assertions;

public interface ItemIndexer {

    String nextItemId();
}
