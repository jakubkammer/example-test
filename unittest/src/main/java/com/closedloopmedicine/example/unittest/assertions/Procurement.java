package com.closedloopmedicine.example.unittest.assertions;

import java.util.List;

public interface Procurement {

    void submitOrders(List<PurchaseOrder> orders);
}
