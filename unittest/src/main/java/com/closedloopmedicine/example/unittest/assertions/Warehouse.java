package com.closedloopmedicine.example.unittest.assertions;

public interface Warehouse {

    boolean hasCapacity(int required);

    void addItem(Item item);

    void removeAllItemsByName(String name);
}
