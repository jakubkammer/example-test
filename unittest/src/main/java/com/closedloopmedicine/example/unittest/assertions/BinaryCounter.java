package com.closedloopmedicine.example.unittest.assertions;

public class BinaryCounter {

    public static BinaryCounter SHARED_INSTANCE = new BinaryCounter();

    public int countBits(int value) {
        int bitCounter = 0;

        for (int bit = 0; bit < 32; bit++) {
            if ((value & 1) == 1) {
                bitCounter++;
            }
            value >>= 1;
        }
        return bitCounter;
    }
}
