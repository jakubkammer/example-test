package com.closedloopmedicine.example.unittest.assertions;

public class Item {

    private String name;
    private String index;

    public Item(String name, String index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public String getIndex() {
        return index;
    }
}
