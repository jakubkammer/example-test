package com.closedloopmedicine.example.unittest.objectmother;

public class MSISDN {

    private final String value;

    public MSISDN(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
