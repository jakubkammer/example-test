package com.closedloopmedicine.example.unittest.objectmother;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Account {

    private AccountOwner owner;

    public List<Service> services;

    public Account(AccountOwner owner) {
        this.owner = owner;
    }

    public void addService(Service service) {
        if (services == null) {
            services = new ArrayList<>();
        }
        services.add(service);
    }

    public List<Service> getServices() {
        if (services == null) {
            return Collections.emptyList();
        }
        return services;
    }

    public void suspend() {

        if (services == null) {
            return;
        }

        services.forEach(Service::suspend);
    }
}
